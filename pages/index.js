import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';

// import { initGA, logPageView  } from '../utils/analytics';


class Index extends React.Component {

  // componentDidMount() {
  //   if (!window.GA_INITIALIZED) {
  //     initGA();
  //     window.GA_INITIALIZED = true;
  //   }
  //   logPageView();
  // }

  render() {
    return (
      <div>
        <meta charSet="utf-8" />
        <title>Daniel Agus Sidabutar - Web and Mobile Developer</title>
        <link rel='stylesheet' type='text/css' href='/static/index.css' />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta property="og:title" content="Daniel Agus Sidabutar - danilags" />
        <meta name="description" content="Daniel Agus Sidabutar - danilags" />
        <style jsx global>{`
          body { 
            background: #05445f url(images/background.gif) repeat-x 20% 5%;
            font-family: Raleway;
            opacity: 0.4;
            margin: 0px;
          }
        `}</style>
        <LandingPage style={{ 
            width: '100%',
            // margin: '0 auto',
            display: 'flex', 
            flexDirection: 'column', 
            textAlign: 'left', 
            fontFamily: 'Futura',
            justifyContent: 'flex-end',
          }}>
            {/* <figure className="swing-danilags" style={{ width: 200, margin: '0 auto' }}>
              <Image style={{ width: '100%', borderRadius: '50%' }} src="/static/images/danilags-daniel-agus-sidabutar.jpg" alt="Daniel Agus Sidabutar" />
            </figure> */}
            <WrapContent>
              <FirstBox>
                BUSINESS PROSPECTUS OF
              </FirstBox>
              <FlexEnd>
                <HeaderTitle>belajarintensif.id</HeaderTitle>
                <p>"Belajar apapun sampai mahir"</p>
              </FlexEnd>
            </WrapContent>
          </LandingPage>
      </div>
    )
  }
}

const WrapContent = styled.div`
  width: 50%;
  margin: 1rem;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  width: -moz-max-content;
  max-width: 100%;
  box-sizing: border-box;
  position: relative;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const FlexEnd = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const HeaderTitle = styled.h1`
  color: #fff;
  font-size: 48px;
  @media only screen 
    and (min-device-width : 320px) 
    and (max-device-width : 480px) { 
      font-size: 16px;
  }
`;

const LandingPage = styled.div`
  background: url(https://sopohelios.com/static/sopo-helios-image-student-compressed.jpg) no-repeat;
  background-size: cover;
  background-position-y: top;
  background-position-x: center;
  min-height: 100vh;
`;

const FirstBox = styled.p`
  background: #209be4;
  width: 50%;
  text-align: center;
  display: table;
  padding: 10px;
  margin: 20px;
  @media only screen 
    and (min-device-width : 320px) 
    and (max-device-width : 480px) { 
      width: 100%;
  }
`;

export default Index;